package com.sekoding.junit5jupiterstarterspring.service;

import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
public class ComputeService {

    public int sum(int... numbers) {
        int total = 0;

        for (int number : numbers) {
            total += number;
        }

        return total;
    }

    public int rectangleArea(int width, int height) {
        int area = width * height;

        return area;
    }

    public int fibonacci(int number) {
        return computeFibonacci(number);
    }

    private int computeFibonacci(int number) {
        if (number <= 0) {
            throw new NoSuchElementException("There is no zero or negative Fibonacci number");
        }

        if (number <= 2) {
            return 1;
        }

        return computeFibonacci(number - 1) + computeFibonacci(number - 2);
    }
}
