package com.sekoding.junit5jupiterstarterspring.dto;

import java.util.Objects;

public class Output {

    private String content;

    public Output(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Output output = (Output) o;
        return Objects.equals(content, output.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }

    @Override
    public String toString() {
        return "Output{" +
            "content='" + content + '\'' +
            '}';
    }
}
