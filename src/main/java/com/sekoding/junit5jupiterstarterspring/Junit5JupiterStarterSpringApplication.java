package com.sekoding.junit5jupiterstarterspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Junit5JupiterStarterSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(Junit5JupiterStarterSpringApplication.class, args);
    }
}
