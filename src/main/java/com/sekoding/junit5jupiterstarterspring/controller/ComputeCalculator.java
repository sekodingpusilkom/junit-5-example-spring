package com.sekoding.junit5jupiterstarterspring.controller;

import com.sekoding.junit5jupiterstarterspring.dto.Output;
import com.sekoding.junit5jupiterstarterspring.service.ComputeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping("/compute")
public class ComputeCalculator {

    private ComputeService computeService;

    @Autowired
    public ComputeCalculator(ComputeService computeService) {
        this.computeService = computeService;
    }

    @GetMapping("/sum/{numbers}")
    public ResponseEntity<Output> sum(@PathVariable("numbers") String numbersCsv) {
        String[] numbersString = numbersCsv.split(",");
        int[] numbers = Arrays.stream(numbersString)
            .mapToInt(Integer::parseInt)
            .toArray();

        int result = computeService.sum(numbers);

        return ResponseEntity.ok(new Output(Integer.toString(result)));
    }
}
