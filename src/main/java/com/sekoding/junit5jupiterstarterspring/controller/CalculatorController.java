package com.sekoding.junit5jupiterstarterspring.controller;

import com.sekoding.junit5jupiterstarterspring.dto.Output;
import com.sekoding.junit5jupiterstarterspring.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

    private CalculatorService calculatorService;

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping("/add/{a}/{b}")
    public ResponseEntity<Output> add(@PathVariable int a, @PathVariable int b) {
        int result = calculatorService.add(a, b);

        return ResponseEntity.ok(new Output(Integer.toString(result)));
    }

    @GetMapping("/subtract/{a}/{b}")
    public ResponseEntity<Output> subtract(@PathVariable int a, @PathVariable int b) {
        int result = calculatorService.subtract(a, b);

        return ResponseEntity.ok(new Output(Integer.toString(result)));
    }

    @GetMapping("/multiply/{a}/{b}")
    public ResponseEntity<Output> multiply(@PathVariable int a, @PathVariable int b) {
        int result = calculatorService.multiply(a, b);

        return ResponseEntity.ok(new Output(Integer.toString(result)));
    }

    @GetMapping("/divide/{a}/{b}")
    public ResponseEntity<Output> divide(@PathVariable int a, @PathVariable int b) {
        try {
            int result = calculatorService.divide(a, b);

            return ResponseEntity.ok(new Output(Integer.toString(result)));
        } catch (ArithmeticException ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new Output(ex.getMessage()));
        }
    }
}
