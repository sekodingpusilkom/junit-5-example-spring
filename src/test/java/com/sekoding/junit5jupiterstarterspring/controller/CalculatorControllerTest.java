package com.sekoding.junit5jupiterstarterspring.controller;

import com.sekoding.junit5jupiterstarterspring.dto.Output;
import com.sekoding.junit5jupiterstarterspring.service.CalculatorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorControllerTest {

    private CalculatorService calculatorService;
    private CalculatorController calculatorController;

    @BeforeEach
    void setUp() {
        calculatorService = Mockito.mock(CalculatorService.class);
        calculatorController = new CalculatorController(calculatorService);
    }

    @Test
    void add_twoNumbers() {
        Mockito.when(calculatorService.add(1, 3)).thenReturn(4);

        ResponseEntity<Output> output = calculatorController.add(1, 3);

        assertEquals(HttpStatus.OK, output.getStatusCode());
        assertEquals("4", output.getBody().getContent());
    }

    /**
     * TODO: Add more tests to verify other methods from CalculatorController!
     */
}
