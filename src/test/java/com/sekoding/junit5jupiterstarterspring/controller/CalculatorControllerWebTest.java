package com.sekoding.junit5jupiterstarterspring.controller;

import com.sekoding.junit5jupiterstarterspring.service.CalculatorService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CalculatorController.class)
public class CalculatorControllerWebTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CalculatorService calculatorService;

    @Test
    void add_twoNumbers_ok() throws Exception {
        Mockito.when(calculatorService.add(1, 2)).thenReturn(3);

        mockMvc.perform(get("/calculator/add/1/2"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content").value("3"));
    }

    /**
     * TODO: Add more tests to verify other methods from CalculatorController!
     */
}
