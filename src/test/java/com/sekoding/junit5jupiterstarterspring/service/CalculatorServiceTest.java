package com.sekoding.junit5jupiterstarterspring.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorServiceTest {

    private CalculatorService calculatorService;

    @BeforeEach
    void setUp() {
        calculatorService = new CalculatorService();
    }

    /**
     * Taken from junit5-jupiter-starter-maven project (https://github.com/junit-team/junit5-samples/tree/main/junit5-jupiter-starter-maven)
     */
    @Test
    @DisplayName("1 + 1 = 2")
    void addsTwoNumbers() {
        assertEquals(2, calculatorService.add(1, 1),
            "1 + 1 should equal 2");
    }

    /**
     * Taken from junit5-jupiter-starter-maven project (https://github.com/junit-team/junit5-samples/tree/main/junit5-jupiter-starter-maven)
     */
    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
        "0,    1,   1",
        "1,    2,   3",
        "49,  51, 100",
        "1,  100, 101"
    })
    void add(int first, int second, int expectedResult) {
        assertEquals(expectedResult, calculatorService.add(first, second));
    }

    /**
     * TODO: Add more tests to verify other methods from CalculatorService!
     */
}

