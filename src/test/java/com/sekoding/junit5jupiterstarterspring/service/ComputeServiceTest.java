package com.sekoding.junit5jupiterstarterspring.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComputeServiceTest {

    private ComputeService computeService;

    @BeforeEach
    void setUp() {
        computeService = new ComputeService();
    }

    @Test
    void sum_positiveIntegers() {
        int[] testCaseValues = new int[]{1, 2, 3, 4, 5};

        int result = computeService.sum(testCaseValues);

        assertEquals(15, result);
    }

    /**
     * TODO: Add more tests to verify other methods from ComputeService!
     */
}

