# JUnit 5 Starter Spring

Proyek ini adalah contoh kode sederhana yang mengilustrasikan contoh
pengembangan _test code_ pada aplikasi Spring Boot sederhana menggunakan
JUnit 5. Contoh dasar kode ini mengadopsi contoh resmi yang disediakan oleh
pengembang JUnit 5 di repositori [`junit-team/junit5-samples`](https://github.com/junit-team/junit5-samples).

## Prasyarat Pengembangan

Sebelum dapat memulai pengembangan dan uji coba, harap persiapkan hal-hal berikut:

- OpenJDK (Java) versi 17
  - Pastikan untuk memasang **JDK**, bukan **JRE**.
- _Integrated Development Environment_ (IDE) atau _text editor_ favorit Anda.
  Disarankan menggunakan IDE [IntelliJ versi Community](https://www.jetbrains.com/idea/download/).

## Pengembangan & Uji Coba Lokal

1. Salin isi _codebase_ proyek ini ke sebuah folder.
2. Masuk ke folder tersebut menggunakan _shell_ sistem operasi yang dipakai.

   ```bash
   $ cd <alamat folder>
   ```

   > Catatan: _Shell_ biasanya merujuk pada aplikasi _terminal_ atau
   > _command prompt_ di sistem operasi. Pengguna Windows dapat menggunakan
   > PowerShell (`pwsh`) atau Command Prompt (`cmd`). Pengguna Mac OS atau
   > distro GNU/Linux seperti Ubuntu, Kali Linux, Fedora, bisa memakai
   > _shell_ `bash`.
3. Buka folder tersebut menggunakan IDE atau _text editor_.
4. Jalankan _test suite_ dengan memanggil perintah Maven berikut di _shell_:

   ```bash
   $ ./mvnw test
   ```
5. Untuk menjalankan aplikasi secara lokal, bisa menggunakan tombol Run yang
   tersedia di IDE, atau memanggil perintah Maven berikut di _shell_:

   ```bash
   $ ./mvnw spring-boot:run
   ```

   Apliaksi akan berjalan dan dapat diakses melalui alamat `http://localhost:8080`.

## Lisensi & Hak Cipta

Kode sumber proyek ini dapat disalin, dimodifikasi, ataupun dibagi secara
terbuka selama mengikuti syarat dan ketentuan yang berlaku dalam lisensi [Eclipse Public License 2.0](./LICENSE).

Hak Cipta (c) 2022 [Pusat Ilmu Komputer Universitas Indonesia](https://pusilkom.ui.ac.id).
